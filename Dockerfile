# FROM node:current-slim
# # MAINTAINER Rina Dwi Hastuti (rina.dwi@vostra.co.id)
# WORKDIR /terraform-learn/nodeProject
# COPY package.json /terraform-learn/nodeProject/

# # COPY js /apps/post-eod-gl/src
# # COPY docker-scripts/* /apps/bin/
# RUN npm install

# ENV PATH=$PATH:/apps/bin

# EXPOSE 3000
# CMD [ "npm", "start" ]

# RUN npm i

# CMD "bash"
# EXPOSE 3000

# Use the official image as a parent image.
FROM node:current-slim

# Set the working directory.
WORKDIR /terraform-learn/nodeProject

# Copy the file from your host to your current location.
COPY package.json .

# Run the command inside your image filesystem.
RUN npm install

# Add metadata to the image to describe which port the container is listening on at runtime.
EXPOSE 8080

# Run the specified command within the container.
CMD [ "npm", "start" ]

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .