# Configure the Docker provider
provider "docker" {
  host = "tcp://127.0.0.1:8080/"
}

# Create a container
resource "docker_container" "tr_demo" {
  image = "${docker_image.terraform_demo.latest}"
  name  = "tr_demo"
}

resource "docker_image" "terraform_demo" {
  name = "terraform_demo:latest"
}
