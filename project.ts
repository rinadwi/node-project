import express from 'express';

const express = require('express');
const app = express();
const port = 8080;

app.get('/', (req, res) => {
  res.send('Test Terraform with express project');
});

app.listen(port, () =>
  console.log(`Hello world app listening on port ${port}!`),
);
